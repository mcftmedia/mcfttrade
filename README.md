# McftTrade

A barter economy solution for servers. Allows users to setup shops and trade items. Adds wallets for special minerals like gold, diamonds, and iron.

This was made to suit the needs of the [Diamond Mine](http://diamondmine.net) server.

**[License](https://diamondmine.net/plugins/license)** -- Issues --  Builds -- Download -- Bukkit Dev