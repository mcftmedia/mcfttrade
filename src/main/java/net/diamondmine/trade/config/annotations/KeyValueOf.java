package net.diamondmine.trade.config.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.HashMap;
import java.util.Map;

import net.diamondmine.trade.config.KeyValueLoader;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface KeyValueOf {

    @SuppressWarnings("rawtypes")
    Class<? extends KeyValueLoader> value();

    @SuppressWarnings("rawtypes")
    Class<? extends Map> type() default HashMap.class;

}
