package net.diamondmine.trade.config.types;

import net.diamondmine.trade.config.Builder;
import net.diamondmine.trade.config.Loader;

public class BooleanLoaderBuilder implements Loader<Boolean>, Builder<Boolean> {

    public Object write(Boolean value) {
        return value;
    }

    public Boolean read(Object value) {
        return valueOf(value);
    }

    public static Boolean valueOf(Object o) {
        if (o == null) {
            return null;
        } else if (o instanceof Boolean) {
            return (Boolean) o;
        } else {
            return null;
        }
    }

}
