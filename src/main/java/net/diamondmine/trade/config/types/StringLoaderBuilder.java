package net.diamondmine.trade.config.types;

import net.diamondmine.trade.config.Builder;
import net.diamondmine.trade.config.Loader;

public class StringLoaderBuilder implements Loader<String>, Builder<String> {

    public Object write(String value) {
        return String.valueOf(value);
    }

    public String read(Object value) {
        return String.valueOf(value);
    }

}
