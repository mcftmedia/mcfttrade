package net.diamondmine.trade.config.types;

import java.util.List;

import net.diamondmine.trade.config.Builder;
import net.diamondmine.trade.config.Loader;

class ListLoaderBuilder implements Loader<List<Object>>, Builder<List<Object>> {

    public Object write(List<Object> value) {
        return value;
    }

    @SuppressWarnings("unchecked")
    public List<Object> read(Object value) {
        if (value instanceof List) {
            return (List<Object>) value;
        } else {
            return null;
        }
    }

}
