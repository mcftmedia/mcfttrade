package net.diamondmine.trade.config.types;

import java.util.Map;

import net.diamondmine.trade.config.Builder;
import net.diamondmine.trade.config.ConfigurationNode;
import net.diamondmine.trade.config.Loader;

public class NodeLoaderBuilder implements Loader<ConfigurationNode>, Builder<ConfigurationNode> {

    public Object write(ConfigurationNode value) {
        return value;
    }

    public ConfigurationNode read(Object value) {
        if (value instanceof Map) {
            return new ConfigurationNode(value);
        } else {
            return null;
        }
    }

}
