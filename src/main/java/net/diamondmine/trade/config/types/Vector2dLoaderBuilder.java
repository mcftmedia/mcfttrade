package net.diamondmine.trade.config.types;

import net.diamondmine.trade.config.Builder;
import net.diamondmine.trade.config.ConfigurationNode;
import net.diamondmine.trade.config.Loader;
import net.diamondmine.trade.util.MapBuilder.ObjectMapBuilder;

import com.sk89q.worldedit.BlockVector2D;
import com.sk89q.worldedit.Vector2D;

public class Vector2dLoaderBuilder implements Loader<Vector2D>, Builder<Vector2D> {

    private final boolean asBlock;

    public Vector2dLoaderBuilder() {
        this(false);
    }

    public Vector2dLoaderBuilder(boolean asBlock) {
        this.asBlock = asBlock;
    }

    public Object write(Vector2D value) {
        return new ObjectMapBuilder().put("x", value.getX()).put("z", value.getZ()).map();
    }

    public Vector2D read(Object value) {
        ConfigurationNode node = new ConfigurationNode(value);
        Double x = node.getDouble("x");
        Double z = node.getDouble("z");

        if (x == null || z == null) {
            return null;
        }

        return asBlock ? new BlockVector2D(x, z) : new Vector2D(x, z);
    }

}
