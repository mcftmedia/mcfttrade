package net.diamondmine.trade;

import java.io.File;

/**
 * Main framework interface.
 * 
 * @author Jon la Cour
 * @see TradeInstance
 */
public interface Trade {

    /**
     * Get the directory used to store configuration.
     * 
     * @return directory used to store configuration
     */
    File getDataDirectory();

}
