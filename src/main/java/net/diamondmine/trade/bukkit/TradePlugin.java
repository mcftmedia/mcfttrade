package net.diamondmine.trade.bukkit;

import java.io.File;
import java.util.logging.Logger;

import net.diamondmine.trade.Trade;
import net.diamondmine.trade.TradeInstance;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class TradePlugin extends JavaPlugin implements Trade {

    private static final Logger logger = Bukkit.getLogger();

    public TradePlugin() {
        TradeInstance.setInstance(this);
    }

    /**
     * Plugin disabled.
     */
    @Override
    public final void onDisable() {
        logger.info("[McftTrade] Version " + getDescription().getVersion() + " is disabled!");
    }

    /**
     * Plugin enabled.
     */
    @Override
    public final void onEnable() {
        logger.info("[McftTrade] Version " + getDescription().getVersion() + " enabled.");
    }

    public File getDataDirectory() {
        return getDataFolder();
    }

}
