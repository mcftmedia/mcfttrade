package net.diamondmine.trade;

/**
 * Holds a global instance of {@link Trade}.
 * 
 * @author Jon la Cour
 */
public class TradeInstance {

    private static Trade trade;

    private TradeInstance() {
    }

    /**
     * Get an instance of McftTrade.
     * 
     * @return an instance of McftTrade
     */
    public static Trade getInstance() {
        if (trade == null) {
            throw new RuntimeException("Uh oh, McftTrade is not loaded as a plugin, but the API is being used!");
        }

        return trade;
    }

    /**
     * Get whether an instance is available.
     * 
     * @return true if there's an instance
     */
    public static boolean hasInstance() {
        return trade != null;
    }

    /**
     * Set the instance of McftTrade. This should only be called by McftTrade
     * itself.
     * 
     * @param trade the instance of McftTrade
     */
    public static void setInstance(Trade trade) {
        TradeInstance.trade = trade;
    }

}
